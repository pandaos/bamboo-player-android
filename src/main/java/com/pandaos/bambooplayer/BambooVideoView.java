package com.pandaos.bambooplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by orenkosto on 3/28/17.
 */

public class BambooVideoView extends VideoView implements MediaPlayer.OnCompletionListener {

    public BambooVideoView(Context context) {
        super(context);
        this.setOnCompletionListener(this);
    }
    public BambooVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnCompletionListener(this);
    }
    public BambooVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setOnCompletionListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getDefaultSize(0, widthMeasureSpec);

        this.resolveAdjustedSize(width, widthMeasureSpec);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        start();
    }
}
