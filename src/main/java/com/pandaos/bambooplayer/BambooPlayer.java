package com.pandaos.bambooplayer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.cast.MediaInfo;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.kaltura.client.enums.KalturaMediaType;
import com.kaltura.playkit.PKDrmParams;
import com.kaltura.playkit.PKMediaConfig;
import com.kaltura.playkit.PKMediaEntry;
import com.kaltura.playkit.PKMediaFormat;
import com.kaltura.playkit.PKMediaSource;
import com.kaltura.playkit.PKPluginConfigs;
import com.kaltura.playkit.PlayKitManager;
import com.kaltura.playkit.Player;
import com.kaltura.playkit.PlayerEvent;
import com.kaltura.playkit.PlayerState;
import com.kaltura.playkit.plugins.ads.AdCuePoints;
import com.kaltura.playkit.plugins.ads.AdEvent;
import com.kaltura.playkit.plugins.googlecast.BasicCastBuilder;
import com.kaltura.playkit.plugins.googlecast.OVPCastBuilder;
import com.kaltura.playkit.plugins.ima.IMAConfig;
import com.kaltura.playkit.plugins.ima.IMAPlugin;
import com.kaltura.playkit.plugins.ovp.KalturaStatsPlugin;
import com.kaltura.playkit.utils.Consts;
import com.pandaos.pvpclient.models.PvpChannelModel;
import com.pandaos.pvpclient.models.PvpChannelModelCallback;
import com.pandaos.pvpclient.models.PvpColors;
import com.pandaos.pvpclient.models.PvpColors_;
import com.pandaos.pvpclient.models.PvpConfigModel;
import com.pandaos.pvpclient.models.PvpConfigModelCallback;
import com.pandaos.pvpclient.models.PvpConfigModel_;
import com.pandaos.pvpclient.models.PvpEntryModel;
import com.pandaos.pvpclient.models.PvpEntryModelCallback;
import com.pandaos.pvpclient.models.PvpEntryModel_;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpHelper_;
import com.pandaos.pvpclient.models.PvpLiveEntryModel;
import com.pandaos.pvpclient.models.PvpLiveEntryModelCallback;
import com.pandaos.pvpclient.models.PvpLiveEntryModel_;
import com.pandaos.pvpclient.models.SharedPreferences_;
import com.pandaos.pvpclient.objects.PvpChannel;
import com.pandaos.pvpclient.objects.PvpChannelScheduleItem;
import com.pandaos.pvpclient.objects.PvpDownloadedEntry;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpLiveEntry;
import com.pandaos.pvpclient.objects.PvpMeta;
import com.pandaos.pvpclient.objects.PvpResult;
import com.pandaos.pvpclient.objects.PvpUser;
import com.pandaos.pvpclient.objects.PvpYoutubeId;
import com.pandaos.pvpclient.utils.PvpConstants;
import com.pandaos.pvpclient.utils.PvpEncryptionHelper;
import com.pandaos.pvpclient.utils.PvpEncryptionHelper_;
import com.pandaos.pvpclient.utils.PvpLocalizationHelper;
import com.pandaos.pvpclient.utils.PvpLocalizationHelper_;
import com.pandaos.pvpclient.utils.PvpNetworkStatusHelper;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerUtils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.androidannotations.annotations.Background;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import fr.bmartel.youtubetv.YoutubeTvView;
import fr.bmartel.youtubetv.listener.IPlayerListener;
import fr.bmartel.youtubetv.model.VideoInfo;
import fr.bmartel.youtubetv.model.VideoState;

import static com.pandaos.bambooplayer.R.id.bamboo_player_image;
import static com.pandaos.bambooplayer.R.id.video_view;

//import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
//import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;

/**
 * Created by orenkosto on 4/2/17.
 */

public class BambooPlayer extends RelativeLayout implements ExoPlayer.EventListener, VideoRendererEventListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, PvpConfigModelCallback, PvpEntryModelCallback, PvpLiveEntryModelCallback, PvpChannelModelCallback, BambooPlaybackControlsView.BambooPlaybackControlsViewInterface, PvpEncryptionHelper.TokenizeUrlCallback {

    public interface BambooPlayerInterface {
        void onPlayerStart();
        void onPlayerPlayPause(boolean isPlaying);
        void onPlayerSeeked();
        void onPlayerComplete();
        void onPlayerError();
        void onPlayerVideoSizeChanged(int width, int height);
        void onYoutubePlayerStateChanged(PlayerConstants.PlayerState state);
        void toggleFullscreen(boolean isFullScreen);
    }

    Context context;
    public static final int TIME_UNSET = Integer.MIN_VALUE + 1;
    public static final int POSITION_UNSET = -1;

    private static final String TAG = "BambooPlayer";
    private static final int WATCH_DURATION_DELAY = 10000;

    private SimpleExoPlayerView videoView;
    private SimpleExoPlayer player;
    private YouTubePlayerView youTubePlayerView;
    private YoutubeTvView youTubePlayerViewTV;
    private ProgressBar progress_bar;
    private ImageView player_image;
    private RelativeLayout bamboo_player_countdown_container;
    private TextView bamboo_player_countdown_text;
    private TextView bamboo_player_countdown_title;
    private TextView bamboo_player_countdown_indicator_days;
    private TextView bamboo_player_countdown_indicator_hours;
    private TextView bamboo_player_countdown_indicator_minutes;
    private TextView bamboo_player_countdown_indicator_seconds;

    public LinearLayout kaltura_player_root;
    public Player kalturaPlayer;
    public YouTubePlayer youTubePlayer;

    public BambooPlaybackControlsView kaltura_player_controls;
    public BambooPlayerDvrControls player_dvr_controls;

    private boolean shouldAutoPlay = true;
    private int resumeWindow;
    private long resumePosition;
    private DataSource.Factory mediaDataSourceFactory;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private static final CookieManager DEFAULT_COOKIE_MANAGER;
    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }
    private DefaultTrackSelector trackSelector;
    private Handler mainHandler;
    private com.pandaos.bambooplayer.EventLogger eventLogger;
    private boolean needRetrySource;

    private int watchDuration;
    private Handler watchDurationHandler = new Handler();
    private Runnable watchDurationRunnable;

    private static boolean useCustomControls = false;

    PvpChannelScheduleItem currentPlayingItem;

    public PvpEntry entry;
    public PvpLiveEntry liveEntry;
    public PvpChannel channel;
    private PvpHelper pvpHelper;
    private SharedPreferences_ preferences;
    private PvpColors pvpColors;
    private PvpEncryptionHelper encryptionHelper;
    private PvpLocalizationHelper pvpLocalizationHelper;
    private PvpConfigModel configModel;
    private PvpEntryModel entryModel;
    private PvpLiveEntryModel liveEntryModel;
    private PvpChannelModel channelModel;
    SharedPreferences sharedPreferences;
    public Uri video;
    private ConstantAnchorMediaController mediaControls;
    public static float videoAspectRatio = 16f / 9f;
    public static final int MAX_RETRIES = 3;
    private static boolean playerStarted = false;

    private static int currentStreamConfigIndex = -1;
    private static int retryCount = 0;
    static Timer retryTimer;
    static TimerTask retryTimerTask;
    static Timer isLiveTimer;
    static TimerTask isLiveTimerTask;
    private CountDownTimer liveEntryTimer;
    private AlertDialog streamEndDialog;

    private static GoogleAnalytics googleAnalytics;
    private static Tracker googleAnalyticsTracker;
    private FirebaseAnalytics mFirebaseAnalytics;

    public BambooPlayerInterface playerInterface;

    //DVR params
    long currentOffsetPosition = 0;
    long lastOffsetCreationTime = 0;
    boolean dvrIsPlay = true;
    String hlsDvrUrl = null;
    String hlsDvrBaseUrl = null;
    String hlsAbrBase = null;
    String hlsAbrStreamNameSuffix = null;
    String hlsDvrStreamNameSuffix = null;

    private PlayerConstants.PlayerState ytPlayerState = PlayerConstants.PlayerState.UNKNOWN;

    public BambooPlayer(Context context) {
        super(context);
        setup(context);
    }

    public BambooPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        String useCustomControlsAttr = attrs.getAttributeValue(PvpConstants.ANDROID_ATTRS_RES_SCHEMA, "useCustomControls");
        if (useCustomControlsAttr != null) {
            useCustomControls = Boolean.parseBoolean(useCustomControlsAttr);
        } else {
            useCustomControls = false;
        }
        setup(context);
    }

    private void setup(Context ctx) {
        this.context = ctx;
        pvpHelper = PvpHelper_.getInstance_(ctx);
        preferences = new SharedPreferences_(ctx);
        pvpColors = PvpColors_.getInstance_(ctx);
        encryptionHelper = PvpEncryptionHelper_.getInstance_(ctx);
        pvpLocalizationHelper = PvpLocalizationHelper_.getInstance_(ctx);
        configModel = PvpConfigModel_.getInstance_(ctx);
        entryModel = PvpEntryModel_.getInstance_(ctx);
        liveEntryModel = PvpLiveEntryModel_.getInstance_(ctx);
        sharedPreferences = context.getApplicationContext().getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE);

        inflateLayout(ctx);

        videoView = findViewById(video_view);
        youTubePlayerView = findViewById(R.id.youtube_player);
        youTubePlayerViewTV = findViewById(R.id.youtube_video_tv);

        progress_bar = findViewById(R.id.progress_bar);
        player_image = findViewById(bamboo_player_image);
        kaltura_player_root = findViewById(R.id.kaltura_player_root);
        kaltura_player_controls = findViewById(R.id.kaltura_player_controls);
        player_dvr_controls = findViewById(R.id.player_dvr_controls);
        bamboo_player_countdown_container = findViewById(R.id.bamboo_player_countdown_container);
        bamboo_player_countdown_text = findViewById(R.id.bamboo_player_countdown_text);
        bamboo_player_countdown_title = findViewById(R.id.bamboo_player_countdown_title);
        bamboo_player_countdown_indicator_days = findViewById(R.id.bamboo_player_countdown_indicator_days);
        bamboo_player_countdown_indicator_hours = findViewById(R.id.bamboo_player_countdown_indicator_hours);
        bamboo_player_countdown_indicator_minutes = findViewById(R.id.bamboo_player_countdown_indicator_minutes);
        bamboo_player_countdown_indicator_seconds = findViewById(R.id.bamboo_player_countdown_indicator_seconds);

        bamboo_player_countdown_title.setText(pvpLocalizationHelper.localizedString(getResources().getString(R.string.bamboo_player_countdown_title)));
        bamboo_player_countdown_indicator_days.setText(pvpLocalizationHelper.localizedString(getResources().getString(R.string.days)));
        bamboo_player_countdown_indicator_hours.setText(pvpLocalizationHelper.localizedString(getResources().getString(R.string.hours)));
        bamboo_player_countdown_indicator_minutes.setText(pvpLocalizationHelper.localizedString(getResources().getString(R.string.minutes)));
        bamboo_player_countdown_indicator_seconds.setText(pvpLocalizationHelper.localizedString(getResources().getString(R.string.seconds)));

        progress_bar.getIndeterminateDrawable().setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR), android.graphics.PorterDuff.Mode.MULTIPLY);

        //set the media controller buttons
        if (mediaControls == null && !pvpHelper.isTvScreen()) {
            mediaControls = new ConstantAnchorMediaController(context, videoView);
        }

        configModel.getConfig(null);
        if (BambooPlayerInterface.class.isAssignableFrom(this.context.getClass())) {
            setPlayerInterface((BambooPlayerInterface) this.context);
        }

        setupGoogleAnalytics(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        //HLS DVR Base URL
        hlsDvrBaseUrl = pvpHelper.getConfig().mobile.containsKey("hlsDvrBaseUrl") ? pvpHelper.getConfig().mobile.get("hlsDvrBaseUrl").toString() : "";
        hlsAbrBase = pvpHelper.getConfig().mobile.containsKey("hlsAbrBase") ? pvpHelper.getConfig().mobile.get("hlsAbrBase").toString() : "";
        hlsAbrStreamNameSuffix = "_abr/";
        hlsDvrStreamNameSuffix = "_dvr/";

        kaltura_player_controls.setBambooPlaybackControlsViewInterface(this);

        youTubePlayerViewTV.addPlayerListener(new IPlayerListener() {

            @Override
            public void onPlayerReady(VideoInfo videoInfo) {
                if (channel != null && !pvpHelper.getCatchUpEnabled() && videoInfo.getVideoId().length() > 0 && !videoInfo.getVideoId().equals("undefined")) {
                    int offset = (int)currentPlayingItem.getSeekOffset(pvpHelper.getServerTimeOffset());
                    youTubePlayerViewTV.seekTo(offset,true);
                }
            }

            @Override
            public void onPlayerStateChange(VideoState state, long position, float speed, float duration, VideoInfo videoInfo) {
                if (state == VideoState.PLAYING) {
                    sendGoogleAnalyticsEvent("Play");
                    resumeWatchDurationAnalytics();
                } else if (state == VideoState.PAUSED) {
                    sendGoogleAnalyticsEvent("Pause");
                    pauseWatchDurationAnalytics();
                } else if (state == VideoState.ENDED) {
                    stopWatchDurationAnalytics();
                }
                ytPlayerState = ytStateFromTVState(state);
                playerInterface.onYoutubePlayerStateChanged(ytPlayerState);
            }
        });

        streamEndDialog = new AlertDialog.Builder(context)
                .setTitle(pvpLocalizationHelper.localizedString(context.getString(R.string.player_stream_ended)))
                .setMessage(pvpLocalizationHelper.localizedString(context.getString(R.string.player_stream_ended_text)))
                .setPositiveButton(pvpLocalizationHelper.localizedString(context.getString(R.string.ok)), (dialog, which) -> {
                    cancelIsLiveTimer();
                    playerInterface.onPlayerError();
                })
                .create();

        watchDurationRunnable = () -> {
            watchDuration = watchDuration + (WATCH_DURATION_DELAY / 1000);
            sendAnalyticsDurationEvent(watchDuration);
            watchDurationHandler.postDelayed(watchDurationRunnable, WATCH_DURATION_DELAY);
        };
    }

    public boolean isFullScreen() {
        return kaltura_player_controls.isFullScreen();
    }

    public void setFullScreen(boolean fullScreen) {
        kaltura_player_controls.setFullScreen(fullScreen);
    }

    private void restartWatchDurationAnalytics() {
        watchDuration = 0;
        watchDurationHandler.removeCallbacks(watchDurationRunnable);
        watchDurationHandler.postDelayed(watchDurationRunnable, WATCH_DURATION_DELAY);
    }

    private void pauseWatchDurationAnalytics() {
        watchDurationHandler.removeCallbacks(watchDurationRunnable);
    }

    private void resumeWatchDurationAnalytics() {
        watchDurationHandler.postDelayed(watchDurationRunnable, WATCH_DURATION_DELAY);
    }

    private void stopWatchDurationAnalytics() {
        watchDuration = 0;
        watchDurationHandler.removeCallbacks(watchDurationRunnable);
    }

    public void seekYoutubeToSeconds(float seconds) {
        if(pvpHelper.isTvScreen()) {
            youTubePlayerViewTV.seekTo((int)seconds);
        } else {
            youTubePlayer.seekTo(seconds);
        }
    }

    private PlayerConstants.PlayerState ytStateFromTVState(VideoState state) {
        switch (state) {
            case UNSTARTED:
                return PlayerConstants.PlayerState.UNSTARTED;
            case ENDED:
                return PlayerConstants.PlayerState.ENDED;
            case PAUSED:
                return PlayerConstants.PlayerState.PAUSED;
            case PLAYING:
                return PlayerConstants.PlayerState.PLAYING;
            case BUFFERING:
                return PlayerConstants.PlayerState.BUFFERING;
            case VIDEO_CUED:
                return PlayerConstants.PlayerState.VIDEO_CUED;
            default:
                return PlayerConstants.PlayerState.UNKNOWN;
        }
    }

    private void setupGoogleAnalytics(Context context) {
        String trackingId = pvpHelper.googleAnalyticsTrackingId();
        if (trackingId != null) {
            googleAnalytics = GoogleAnalytics.getInstance(context);
            googleAnalyticsTracker = googleAnalytics.newTracker(trackingId);
        }
    }

    public void sendGoogleAnalyticsEvent(String action) {
        if (googleAnalyticsTracker != null) {
            googleAnalyticsTracker.send(new HitBuilders
                    .EventBuilder()
                    .setCategory("BambooPlayer")
                    .setAction(action)
                    .setValue(1)
                    .build());
        }

        if (mFirebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            bundle.putString("user_udid", preferences.userDeviceId().get());
            bundle.putString("userId", pvpHelper.getCurrentUser() != null ? pvpHelper.getCurrentUser().id : null);
            bundle.putString("action", action);
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getCurrentEntryId() + "|" + getCurrentEntryName());
            mFirebaseAnalytics.logEvent("bamboo_player_events", bundle);
        }
    }

    public void sendAnalyticsDurationEvent(int value) {
        if (mFirebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            bundle.putString("user_udid", preferences.userDeviceId().get());
            bundle.putString("userId", pvpHelper.getCurrentUser() != null ? pvpHelper.getCurrentUser().id : null);
            bundle.putString("action", "watch_duration");
            bundle.putInt(FirebaseAnalytics.Param.VALUE, 10);
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getCurrentEntryId() + "|" + getCurrentEntryName());
            mFirebaseAnalytics.logEvent("bamboo_player_events", bundle);
        }
    }

    private String getCurrentEntryId() {
        if (this.entry != null) {
            return entry.id;
        } else if (this.liveEntry != null) {
            return liveEntry.id;
        } else if (this.channel != null) {
            return channel.getId();
        }
        return "";
    }

    private String getCurrentEntryName() {
        if (this.entry != null) {
            return entry.name;
        } else if (this.liveEntry != null) {
            return liveEntry.name;
        } else if (this.channel != null) {
            return channel.name;
        }
        return "";
    }

    public void inflateLayout(Context ctx) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.bamboo_player, this, true);
    }

    public void playEntry(PvpEntry entry) {
        if (entry.mediaType == KalturaMediaType.LIVE_STREAM_FLASH.hashCode) {
            PvpLiveEntry liveEntry = PvpLiveEntry.initFromEntry(entry);
            playLiveEntry(liveEntry);
        } else {
            this.entry = entry;
            this.liveEntry = null;
            this.channel = null;
            stop();

            Uri thumbnailUri = null;
            if(pvpHelper.isTvScreen()) {
                thumbnailUri = entry.generateThumbnailURL(this.context, 2560, 1440, 3);
            } else {
                thumbnailUri = entry.generateThumbnailURL(this.context, 800, 450, 3);
            }
            showImage(thumbnailUri.toString());

            if (pvpHelper.getConfig() != null) {
                startVideo();
//        startRetryTimer();
            } else {
                configModel.getConfig(this);
            }
        }
    }

    public void playDownloadEntry(PvpDownloadedEntry downloadedEntry) {
        stop();
        showImage(downloadedEntry.getThumbnailLocalPath());
        startDownloadedVideo(downloadedEntry.getVideoLocalPath());
    }

    private void startDownloadedVideo(String videoUrl) {
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "yourApplicationName"));
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .setExtractorsFactory(new DefaultExtractorsFactory())
                .createMediaSource(Uri.parse(videoUrl));
        TrackSelection.Factory adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
        trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);
        eventLogger = new com.pandaos.bambooplayer.EventLogger(trackSelector);

        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector, new DefaultLoadControl());
        player.prepare(mediaSource);
        player.addListener(this);
        player.addListener(eventLogger);
        player.setAudioDebugListener(eventLogger);
        player.setVideoDebugListener(this);
        player.setMetadataOutput(eventLogger);

        videoView.setPlayer(player);
        videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        kaltura_player_controls.setPlayer(videoView);
        player.setPlayWhenReady(shouldAutoPlay);
    }

    public void playEntry(String entryId) {
        entryModel.getEntry(entryId, this);
    }

    public void playLiveEntry(final PvpLiveEntry liveEntry) {
        if (liveEntry.mediaType == KalturaMediaType.VIDEO.hashCode) {
            PvpEntry videoEntry = PvpEntry.initFromLiveEntry(liveEntry);
            playEntry(videoEntry);
        } else {
            this.liveEntry = liveEntry;
            this.entry = null;
            this.channel = null;

            Uri thumbnailUri;
            if(pvpHelper.isTvScreen()) {
                thumbnailUri = liveEntry.generateThumbnailURL(this.context, 2560, 1440, 3);
            } else {
                thumbnailUri = liveEntry.generateThumbnailURL(this.context, 800, 450, 3);
            }

            showImage(thumbnailUri.toString());
            stop();
            long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();

            if (liveEntry.startDate > currentTime && !pvpHelper.getCurrentUser().bypassSchedulingRestriction()) {
                long timeLeft = liveEntry.startDate - currentTime;
                bamboo_player_countdown_text.setText(String.format(Locale.ENGLISH, "%02d:%02d:%02d:%02d",
                        TimeUnit.SECONDS.toDays(timeLeft),
                        TimeUnit.SECONDS.toHours(timeLeft) % 24,
                        TimeUnit.SECONDS.toMinutes(timeLeft) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(timeLeft)),
                        TimeUnit.SECONDS.toSeconds(timeLeft) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(timeLeft))));
                bamboo_player_countdown_container.setVisibility(VISIBLE);

                liveEntryTimer = new CountDownTimer(timeLeft * 1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        bamboo_player_countdown_text.setText(String.format(Locale.ENGLISH, "%02d:%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toDays(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24,
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                    }

                    @Override
                    public void onFinish() {
                        playLiveEntry(liveEntry);
                    }
                };
                liveEntryTimer.start();
            } else {
                if (liveEntry.endDate > 0 && liveEntry.endDate < currentTime) {
                    streamEnded();
                } else if (!pvpHelper.isCountryAllowed()
                        && !pvpHelper.getCurrentUser().bypassCountryRestriction()
                        && !pvpHelper.getCurrentUser().role.equals(PvpUser.userRole.adminRole.toString())
                        && !pvpHelper.getCurrentUser().role.equals(PvpUser.userRole.rootRole.toString())
                        && !pvpHelper.getCurrentUser().role.equals(PvpUser.userRole.managerRole.toString())) {
                    System.out.println("blocked by country");
                    new AlertDialog.Builder(context)
                            .setTitle(pvpLocalizationHelper.localizedString(context.getString(R.string.player_country_blocked_dialog)))
                            .setMessage(pvpLocalizationHelper.localizedString(context.getString(R.string.player_country_blocked_dialog_text)))
                            .setPositiveButton(pvpLocalizationHelper.localizedString(context.getString(R.string.ok)), (dialog, which) -> playerInterface.onPlayerError())
                            .setCancelable(false)
                            .show();
                } else {
                    bamboo_player_countdown_container.setVisibility(GONE);
                    if (pvpHelper.getConfig() != null) {
                        startVideo();
                        if (!pvpHelper.isAlwaysLive()) {
                            startIsLiveTimer();
                        }
                        if (!pvpHelper.isDrmEnabled()) {
//                            startRetryTimer();
                        }
                    } else {
                        configModel.getConfig(this);
                    }
                }
            }
        }
    }

    public void playLiveEntry(String entryId) {
        liveEntryModel.getLiveEntry(entryId, this);
    }

    public void playChannel(String channelId) {
        this.channelModel.getChannel(channelId, this);
    }

    @UiThread
    public void playChannel(PvpChannel channel) {
        this.entry = null;
        this.liveEntry = null;
        this.channel = channel;

        Uri thumbnailUri = null;
        if(pvpHelper.isTvScreen()) {
            thumbnailUri = channel.entry.generateThumbnailURL(this.context, 2560, 1440, 3);
        } else {
            thumbnailUri = channel.entry.generateThumbnailURL(this.context, 800, 450, 3);
        }

        showImage(thumbnailUri.toString());
        if(isPlaying()) {
            //If we're in middle of playback we stop - maybe remove entirely.
            stop();
        }

        if (pvpHelper.getConfig() != null) {
            startVideo();
            if (!pvpHelper.isDrmEnabled()) {
//                startRetryTimer();
            }
        } else {
            Log.e("BambooPlayer", "Why get config from player");
            configModel.getConfig(this);
        }
    }

    @UiThread
    public void playChannel(PvpChannel channel, int position) {

        Boolean isCurrentChannel = this.channel == channel;
        this.channel = channel;

        currentPlayingItem = channel.getScheduleItems().get(position);
        PvpYoutubeId youtubeID = channel.getScheduledYoutubeId(position);
        if (channel.isYoutubeChannel() && youtubeID != null) {
            stopNativePlayer();
            startYoutubeVideo(youtubeID, currentPlayingItem.getSeekOffset(pvpHelper.getServerTimeOffset()));
        } else {
            if (!isCurrentChannel) {
                playChannel(channel);
            }
            long offset = currentPlayingItem.getSeekOffset(pvpHelper.getServerTimeOffset());
            if (offset > 0) {
                setDvrRewindToItem(offset);
            }
        }
    }

    public void pause() {
        if (videoView != null && videoView.getPlayer() != null) {
            videoView.getPlayer().setPlayWhenReady(false);
            updateResumePosition();
        }
        if (kalturaPlayer != null && kalturaPlayer.isPlaying()) {
            try {
                kalturaPlayer.pause();
                kalturaPlayer.getCurrentPosition();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (ytPlayerState == PlayerConstants.PlayerState.PLAYING){
            if (pvpHelper.isTvScreen() && youTubePlayerViewTV != null) {
                youTubePlayerViewTV.playPause();
            } else if (youTubePlayer != null) {
                youTubePlayer.pause();
            }
        }

        kaltura_player_controls.showControls(true);
    }

    public void resume() {
        if (videoView != null && videoView.getPlayer() != null && !videoView.getPlayer().getPlayWhenReady()) {
            if (kaltura_player_controls.nativeMediaPlayer != null) {
                kaltura_player_controls.nativeMediaPlayer.start();
            } else {
                videoView.getPlayer().setPlayWhenReady(true);
            }
        }
        if (kalturaPlayer != null && !kalturaPlayer.isPlaying()) {
            try {
                kalturaPlayer.play();
                kaltura_player_controls.showControls(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (ytPlayerState != PlayerConstants.PlayerState.UNKNOWN && ytPlayerState != PlayerConstants.PlayerState.PLAYING) {
            if (pvpHelper.isTvScreen() && youTubePlayerViewTV != null) {
                youTubePlayerViewTV.playPause();
            } else if (youTubePlayer != null) {
                youTubePlayer.play();
            }
        }
    }

    public void stop() {
        Log.d("BambooPlayer", "In stop");
        if (liveEntryTimer != null) {
            liveEntryTimer.cancel();
            liveEntryTimer = null;
        }
        if (videoView != null) {
            stopNativePlayer();
        }
        if (kalturaPlayer != null) {
            try {
                kalturaPlayer.pause();
                kalturaPlayer.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
            kaltura_player_root.removeView(kalturaPlayer.getView());
            kalturaPlayer = null;
            kaltura_player_controls.release();
        }

        if(youTubePlayer != null) {
            youTubePlayer.pause();
        }

        if (pvpHelper.isTvScreen() && youTubePlayerViewTV != null) {
            Log.e("YTPlayer", "Stopping YT player");
            youTubePlayerViewTV.pause();
        }

        cancelRetryTimer();
        cancelIsLiveTimer();
    }

    public void setDvrHlsOffsetUrl() {
        try {
            String url = currentHlsStreamUrl();
            if (url.indexOf(hlsDvrStreamNameSuffix) > -1) {
                hlsDvrUrl = url.split("-")[0];
                hlsDvrUrl = hlsDvrUrl + "-" + currentOffsetPosition + ".m3u8";
            } else if (url.indexOf(hlsAbrStreamNameSuffix) > -1) {
                String streamName = url.split(hlsAbrBase)[1];
                streamName = streamName.split(hlsAbrStreamNameSuffix)[0];
                hlsDvrUrl = hlsDvrBaseUrl.replaceFirst("STREAM_NAME", streamName);
                hlsDvrUrl = hlsDvrUrl.replaceFirst("OFFSET", Long.toString(currentOffsetPosition));
            }

            setVideo(hlsDvrUrl);
        } catch (Exception e) {

        }
    }

    public void cleanDVR(){
        hlsDvrUrl = null;
    }

    public String hlsOffsetText(long offsetInSec) {
        long hours = offsetInSec / 3600;
        long secondsLeft = offsetInSec - hours * 3600;
        long minutes = secondsLeft / 60;
        long seconds = secondsLeft - minutes * 60;
        String formattedTime = "";

        if(hours > 0) {
            if (hours < 10) {
                formattedTime += "0";
            }

            formattedTime += hours + ":";
        }

        if (minutes < 10) {
            formattedTime += "0";
        }

        formattedTime += minutes + ":";

        if (seconds < 10) {
            formattedTime += "0";
        }

        formattedTime += seconds ;

        if(hours > 0) {
            formattedTime += context.getString(R.string.hours_ago);
        } else {
            formattedTime += context.getString(R.string.minutes_ago);
        }

        return formattedTime;
    }

    public long getCurrentOffsetPosition(){
        return currentOffsetPosition;
    }

    public void setOriginalHlsUrl() {
        if (liveEntry != null) {
            hlsDvrUrl = liveEntry.hlsStreamUrl;
            currentOffsetPosition = 0;
            setVideo(hlsDvrUrl);
        }
    }

    @Override
    public void setDvrForward(){
        boolean displayLiveIndicator = true;

        if(currentOffsetPosition > 0) {
            currentOffsetPosition = currentOffsetPosition - 30;

            if(currentOffsetPosition < 0) {
                currentOffsetPosition = 0;
                setOriginalHlsUrl();
                kaltura_player_controls.setSeekBar(false);

            } else {
                setDvrHlsOffsetUrl();
                kaltura_player_controls.setSeekBar(true);
                displayLiveIndicator = false;
            }

            startNativePlayer();
            if (currentOffsetPosition == 0) {
                kaltura_player_controls.setIsLive(true);
            } else {
                kaltura_player_controls.setIndicatorText(hlsOffsetText(currentOffsetPosition), displayLiveIndicator);
            }
        } else {
            currentOffsetPosition = 0;
            if (liveEntry != null) {
                setOriginalHlsUrl();
            } else {
                hlsDvrUrl = currentHlsStreamUrl();
                setVideo(hlsDvrUrl);
            }
            kaltura_player_controls.setSeekBar(false);
            kaltura_player_controls.setIsLive(true);
        }
        kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.FORWARD);
        kaltura_player_controls.showControls(true);
        player_dvr_controls.setControlsType(BambooPlayerDvrControls.ControlsType.DVR_FORWARD);
        player_dvr_controls.showControls(true);
    }

    private String currentHlsStreamUrl() {
        if (liveEntry != null) {
            return liveEntry.hlsStreamUrl;
        }
        if (channel != null && channel.entry != null) {
            return channel.entry.hlsStreamUrl;
        }
        return "";
    }

    public void setDvrRewindToItem(long offset) {
        currentOffsetPosition = offset;
        setDvrHlsOffsetUrl();
        startNativePlayer();
        kaltura_player_controls.setSeekBar(true);
        kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.REWIND);
        kaltura_player_controls.showControls(true);
        if (currentOffsetPosition == 0) {
            kaltura_player_controls.setIsLive(true);
        } else {
            kaltura_player_controls.setIndicatorText(hlsOffsetText(currentOffsetPosition), false);
        }
        player_dvr_controls.setControlsType(BambooPlayerDvrControls.ControlsType.DVR_REWIND);
        player_dvr_controls.showControls(true);
    }

    @Override
    public void setDvrRewind() {
        setDvrRewindToItem(currentOffsetPosition + 30);
    }

    @Override
    public void playOrPauseVideoDVR(){
        if(dvrIsPlay) {
            lastOffsetCreationTime = System.currentTimeMillis() / 1000;

            if(videoView != null) {
                videoView.getPlayer().setPlayWhenReady(false);
            }

            dvrIsPlay = false;
            kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.PAUSE);
            player_dvr_controls.setControlsType(BambooPlayerDvrControls.ControlsType.DVR_PAUSE);
        } else {
            currentOffsetPosition = currentOffsetPosition + ((System.currentTimeMillis() / 1000) - lastOffsetCreationTime);
            setDvrHlsOffsetUrl();
            startVideo();

            if(videoView != null) {
                videoView.getPlayer().setPlayWhenReady(true);
            }

            dvrIsPlay = true;
            kaltura_player_controls.setSeekBar(true);

            if (currentOffsetPosition == 0) {
                kaltura_player_controls.setIsLive(true);
            } else {
                kaltura_player_controls.setIndicatorText(hlsOffsetText(currentOffsetPosition), false);
            }

            kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.PLAY);
            player_dvr_controls.setControlsType(BambooPlayerDvrControls.ControlsType.DVR_PLAY);
        }
        kaltura_player_controls.showControls(true);
        player_dvr_controls.showControls(true);
    }

    @Override
    public void toggleFullScreenMode(boolean isFullscreen) {
        playerInterface.toggleFullscreen(isFullscreen);
    }

    public void fastForward() {
        kaltura_player_controls.seekBy(15);
        kaltura_player_controls.showControls(true);
    }

    public void rewind() {
        kaltura_player_controls.seekBy(-15);
        kaltura_player_controls.showControls(true);
    }

    private void stopNativePlayer() {
        try {
            if (kaltura_player_controls.nativeMediaPlayer != null) {
                kaltura_player_controls.nativeMediaPlayer.pause();
            } else {

                if(videoView.getPlayer() != null) {
                    videoView.getPlayer().setPlayWhenReady(false);
                }

                updateResumePosition();
                if(player != null) {
                    player.release();
                    player = null;
                }
                trackSelector = null;
                eventLogger = null;
            }
        } catch (Exception e) {
            Log.e(getClass().getName(), "Exception: " + e.toString());
            e.printStackTrace();
        }
    }

    public boolean isPlaying() {
        boolean result = false;
        if (videoView != null && videoView.getPlayer() != null) {
            result = videoView.getPlayer().getPlayWhenReady();
        }
        if (!result && kalturaPlayer != null) {
            result = kalturaPlayer.isPlaying();
        }
        if (!result && youTubePlayer!= null) {
            result = (ytPlayerState == PlayerConstants.PlayerState.PLAYING);
        }
        if (!result && pvpHelper.isTvScreen() && youTubePlayerViewTV != null) {
            result = (ytPlayerState == PlayerConstants.PlayerState.PLAYING);
        }
        return result;
    }

    protected void startVideo() {
        resetCurrentStreamConfigIndex();
        startVideo(currentStreamConfigIndex);
    }

    void setVideo(String videoUrl) {
        if(pvpHelper.isTokenizationEnable() && entry == null) {
            encryptionHelper.tokenizeUrl(videoUrl, this);
        } else  {
            tokenizeUrlCallback(videoUrl);
        }

    }

    public void tokenizeUrlCallback(String tokenizeUrl) {
        Log.d("Tokenized URL", tokenizeUrl);
        video = Uri.parse(tokenizeUrl);
        if(entry != null) {
            startKalturaPlayer();
            kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.DEFAULT);
        } else if (liveEntry != null || channel != null) {
            if (currentHlsStreamUrl().equals(tokenizeUrl)) {
                currentOffsetPosition = 0;
            }
            startNativePlayer();
            kaltura_player_controls.setControlsType(BambooPlaybackControlsView.ControlsType.LIVE);
            if (currentOffsetPosition == 0) {
                kaltura_player_controls.setIsLive(true);
            } else {
                kaltura_player_controls.setIndicatorText(hlsOffsetText(currentOffsetPosition), true);
            }
        }
    }

    private void loadYoutubeVideo(String videoId, float startSecond) {
        Lifecycle lifecycle = null;
        Context context = getContext();
        if (context instanceof AppCompatActivity) {
            lifecycle = ((AppCompatActivity) context).getLifecycle();
        }
        YouTubePlayerUtils.loadOrCueVideo(youTubePlayer, lifecycle, videoId, startSecond);
    }

    private void startYoutubeVideo(PvpYoutubeId pvpYoutubeId, long seekToSeconds) {
        hideProgress();
        videoView.setVisibility(View.GONE);
        if (pvpHelper.isTvScreen()) {
            youTubePlayerView.setVisibility(View.GONE);
            youTubePlayerViewTV.setVisibility(View.VISIBLE);
            youTubePlayerViewTV.playVideo(pvpYoutubeId.id);
        } else {
            youTubePlayerViewTV.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.VISIBLE);

            if (youTubePlayer == null) {
                youTubePlayerView.initialize(new AbstractYouTubePlayerListener() {

                    @Override
                    public void onStateChange(@NonNull YouTubePlayer player,
                                              @NonNull PlayerConstants.PlayerState state) {
                        super.onStateChange(player, state);

                        if (state == PlayerConstants.PlayerState.PLAYING) {
                            hideProgress();
                        }

                        ytPlayerState = state;
                        playerInterface.onYoutubePlayerStateChanged(state);
                    }

                    @Override
                    public void onVideoId(@NonNull YouTubePlayer player,
                                          @NonNull String videoId) {
                        super.onVideoId(player, videoId);
                        showProgress();
                    }

                    @Override
                    public void onReady(@NonNull YouTubePlayer player) {
                        super.onReady(player);
                        youTubePlayer = player;

                        if (!pvpHelper.getCatchUpEnabled()) {
                            View liveControls = youTubePlayerView.inflateCustomPlayerUi(R.layout.youtube_live_playback_controls);
                            player.addListener(new YoutubeLiveController(liveControls, player));
                        }

                        loadYoutubeVideo(pvpYoutubeId.id, seekToSeconds);
                    }
                });
            } else {
                loadYoutubeVideo(pvpYoutubeId.id, seekToSeconds);
            }
        }
    }

    @UiThread
    public void startVideo(final int streamConfigIndex) {
        youTubePlayerView.setVisibility(View.GONE);
        youTubePlayerViewTV.setVisibility(View.GONE);
        showProgress();
        playerStarted = false;

        try {
            if (entry != null) {
                if (streamConfigIndex > -1) {
                    setVideo(entry.dataUrl);
                } else if (pvpHelper.isDrmEnabled()) {
                    setVideo(getMpegDashUrl(entry.id));
                } else {
                    String videoUrl = getHlsUrl(entry.id);
                    if (entry.status == 7 && entry.info != null && entry.info.hlsUrl != null && !entry.info.hlsUrl.isEmpty()) {
                        videoUrl = entry.info.hlsUrl;
                    }
                    setVideo(videoUrl);
                }
            } else  if (channel != null) {
                PvpYoutubeId youtubeId = channel.getScheduledYoutubeId(pvpHelper.getServerTimeOffset());
                if (channel.isYoutubeChannel() && youtubeId!=null) {
                    stopNativePlayer();
                    currentPlayingItem = channel.getCurrentItem(pvpHelper.getServerTimeOffset());
                    startYoutubeVideo(youtubeId, currentPlayingItem.getSeekOffset(pvpHelper.getServerTimeOffset()));
                } else {
                    String videoUrl = pvpHelper.isDrmEnabled() ? getMpegDashUrl(channel.entry.id) : channel.hlsUrl;
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        videoUrl = videoUrl.replace("https://", "http://");
                    }
                    setVideo(videoUrl);
                }
            } else if (liveEntry != null) {
                String videoUrl = hlsDvrUrl != null ? hlsDvrUrl : liveEntry.hlsStreamUrl;
                if (pvpHelper.isDrmEnabled()) {
                    if (liveEntry.liveStreamConfigurations.size() > 0) {
                        videoUrl = liveEntry.liveStreamConfigurations.get(0).url;
                    } else {
                        videoUrl = getMpegDashUrl(liveEntry.id);
                    }
                } else {
                    if (streamConfigIndex > -1) {
                        if (liveEntry.liveStreamConfigurations.size() > streamConfigIndex) {
                            videoUrl = liveEntry.liveStreamConfigurations.get(streamConfigIndex).url;
                        }
                    }
                }

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    videoUrl = videoUrl.replace("https://", "http://");
                }

                setVideo(videoUrl);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    public void startNativePlayer() {
        new Handler(Looper.getMainLooper()).post(() -> {
            kaltura_player_root.setVisibility(GONE);
            if (pvpHelper.isTvScreen()) {
                kaltura_player_controls.setVisibility(GONE);
            } else {
                kaltura_player_controls.setVisibility(VISIBLE);
            }
            videoView.setVisibility(VISIBLE);
        });

        //set the media controller in the VideoView
        if (!pvpHelper.isTvScreen()) {
//          videoView.setMediaController(mediaControls);
            kaltura_player_controls.setPlayer(videoView);
        }
        kaltura_player_controls.setPlayer(videoView);

        clearResumePosition();
        mediaDataSourceFactory = buildDataSourceFactory(true);
        mainHandler = new Handler();
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        videoView.requestFocus();

        boolean needNewPlayer = player == null;

        String entryId = "";
        if (entry != null) {
            entryId = entry.id;
        } else if (liveEntry != null) {
            entryId = liveEntry.id;
        } else if (channel != null) {
            entryId = channel.entry.id;
        }

        if (needNewPlayer) {
            TrackSelection.Factory adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);
//                    trackSelectionHelper = new TrackSelectionHelper(trackSelector, adaptiveTrackSelectionFactory);
//                    lastSeenTrackGroupArray = null;
            eventLogger = new com.pandaos.bambooplayer.EventLogger(trackSelector);
            UUID drmSchemeUuid = UUID.fromString(PvpConstants.DRM_SCHEME_WIDEVINE);
            DrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;
            if (drmSchemeUuid != null && pvpHelper.isDrmEnabled()) {
                String drmLicenseUrl = getDrmLicenseUrl(entryId);
                String[] keyRequestPropertiesArray = new String[0];
                try {
                    drmSessionManager = buildDrmSessionManager(drmSchemeUuid, drmLicenseUrl,
                            keyRequestPropertiesArray);
                } catch (UnsupportedDrmException e) {
                    //TODO: error
                    int errorStringId = Util.SDK_INT < 18 ? R.string.error_drm_not_supported
                            : (e.reason == UnsupportedDrmException.REASON_UNSUPPORTED_SCHEME
                            ? R.string.error_drm_unsupported_scheme : R.string.error_drm_unknown);
                    System.out.println(context.getString(errorStringId));
                    return;
                }
            }

            LoadControl loadControl = new DefaultLoadControl();

            player = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl, drmSessionManager);

            player.addListener(this);
            player.addListener(eventLogger);
            player.setAudioDebugListener(eventLogger);
            player.setVideoDebugListener(this);
            player.setMetadataOutput(eventLogger);

            videoView.setPlayer(player);
            videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
            player.setPlayWhenReady(shouldAutoPlay);
        }

        MediaSource mediaSource = null;

        if(video != null) {
            mediaSource = buildMediaSource(video);
        }

        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
        if (haveResumePosition) {
            player.seekTo(resumeWindow, resumePosition);
        }

        player.prepare(mediaSource, !haveResumePosition, false);
    }

    private MediaSource buildMediaSource(Uri uri) {
        int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_SS:
//                return new SsMediaSource(uri, buildDataSourceFactory(false),
//                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger);
            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), mainHandler, eventLogger);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, eventLogger);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                        mainHandler, eventLogger);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    private DrmSessionManager<FrameworkMediaCrypto> buildDrmSessionManager(UUID uuid,
                                                                           String licenseUrl, String[] keyRequestPropertiesArray) throws UnsupportedDrmException {
        if (Util.SDK_INT < 18) {
            return null;
        }
        HttpMediaDrmCallback drmCallback = new HttpMediaDrmCallback(licenseUrl,
                buildHttpDataSourceFactory(false));
        if (keyRequestPropertiesArray != null) {
            for (int i = 0; i < keyRequestPropertiesArray.length - 1; i += 2) {
                drmCallback.setKeyRequestProperty(keyRequestPropertiesArray[i],
                        keyRequestPropertiesArray[i + 1]);
            }
        }
        return new DefaultDrmSessionManager<>(uuid,
                FrameworkMediaDrm.newInstance(uuid), drmCallback, null, mainHandler, eventLogger);
    }

    @Override
    public void onVideoEnabled(DecoderCounters counters) {}

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {}

    @Override
    public void onVideoInputFormatChanged(Format format) {}

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {}

    @Override
    public void onRenderedFirstFrame(Surface surface) {}

    @Override
    public void onVideoDisabled(DecoderCounters counters) {}

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        videoAspectRatio = (float) width / (float) height;
        if (playerInterface != null) {
            playerInterface.onPlayerVideoSizeChanged(width, height);
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int time) {
        System.out.println("timeline changed");
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        System.out.println("tracks changed");
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        if (isLoading) {
            System.out.println("loading changed: true");
//            showProgress();
        } else {
            System.out.println("loading changed: false");
//            hideProgress();
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            System.out.println("player state changed - Ended");
            kaltura_player_controls.setPlayerState(PlayerState.IDLE);
            kaltura_player_controls.showControls(true);
            stopWatchDurationAnalytics();
        } else if (playbackState == ExoPlayer.STATE_READY) {
            System.out.println("player state changed - Ready");
            hideProgress();
            hideImage();
            retryCount = 0;
            resetCurrentStreamConfigIndex();
            kaltura_player_controls.setPlayerState(PlayerState.READY);
            if (playerInterface != null) {
                if (!playerStarted) {
                    playerStarted = true;
                    playerInterface.onPlayerStart();
                    sendGoogleAnalyticsEvent("Play");
                    restartWatchDurationAnalytics();
                } else {
                    if (playWhenReady) {
                        sendGoogleAnalyticsEvent("Play");
                        resumeWatchDurationAnalytics();
                    } else {
                        sendGoogleAnalyticsEvent("Pause");
                        pauseWatchDurationAnalytics();
                    }
                }
                playerInterface.onPlayerPlayPause(playWhenReady);
            }
        } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
            System.out.println("player state changed - Buffering");
            kaltura_player_controls.setPlayerState(PlayerState.BUFFERING);
            showProgress();
        } else if (playbackState == ExoPlayer.STATE_IDLE) {
            System.out.println("player state changed - Idle");
            kaltura_player_controls.setPlayerState(PlayerState.IDLE);
        }
        cancelRetryTimer();
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        String errorString = null;
        if (error.type == ExoPlaybackException.TYPE_RENDERER) {
            Exception cause = error.getRendererException();
            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorString = context.getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = context.getString(R.string.error_no_secure_decoder,
                                decoderInitializationException.mimeType);
                    } else {
                        errorString = context.getString(R.string.error_no_decoder,
                                decoderInitializationException.mimeType);
                    }
                } else {
                    errorString = context.getString(R.string.error_instantiating_decoder,
                            decoderInitializationException.decoderName);
                }
            }
        }
        if (errorString != null) {
            System.out.println("Player Error: " + errorString);
        }
        needRetrySource = true;
        if (isBehindLiveWindow(error)) {
            clearResumePosition();
            handlePlayerError(null, 0, 0);
        } else {
            updateResumePosition();
            kaltura_player_controls.showControls(true);
        }
    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        System.out.println("position discontinuity");
        if (needRetrySource) {
            updateResumePosition();
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void startIsLiveTimer() {
        if(isLiveTimer == null) {
            isLiveTimer = new Timer(context.getString(R.string.is_live_timer));


        isLiveTimerTask = new TimerTask() {
            @Override
            public void run() {
                checkIfLive();
            }
        };

        isLiveTimer.schedule(isLiveTimerTask, 10000, 30000);
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {
        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
            retryCount = 0;
            resetCurrentStreamConfigIndex();
            hideProgress();
            hideImage();
            cancelRetryTimer();

            if (playerInterface != null) {
                if (!playerStarted) {
                    playerStarted = true;
                    playerInterface.onPlayerStart();
                    sendGoogleAnalyticsEvent("Play");
                    restartWatchDurationAnalytics();
                }
                playerInterface.onPlayerPlayPause(true);
            }
            kaltura_player_controls.setPlayerState(PlayerState.READY);

            return true;
        } else if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START) {
            showProgress();
        } else if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END) {
            hideImage();
            hideProgress();
        }
        return false;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        handlePlayerError(mp, what, extra);
        return true;
    }

    void handlePlayerError(MediaPlayer mp, int what, int extra) {
//        if (retryCount > MAX_RETRIES) {
//            if (playerInterface != null) {
//                playerInterface.onPlayerError();
//            }
//        } else {
//            currentStreamConfigIndex++;
//            if (entry != null) {
//                if (currentStreamConfigIndex > 0) {
//                    resetCurrentStreamConfigIndex();
//                    retryCount++;
//                }
//                stopNativePlayer();
//                startVideo(currentStreamConfigIndex);
//            } else if (liveEntry != null) {
//                //if there are no more stream configurations to try
//                if (liveEntry.liveStreamConfigurations.size() <= currentStreamConfigIndex) {
//                    resetCurrentStreamConfigIndex();
//                    retryCount++;
//                }
//                stopNativePlayer();
//                startVideo(currentStreamConfigIndex);
//            } else if (channel != null) {
//                retryCount++;
//                stopNativePlayer();
//                startVideo();
//            } else {
//                cancelRetryTimer();
//                if (playerInterface != null) {
//                    playerInterface.onPlayerError();
//                }
//            }
//        }
    }

    void resetCurrentStreamConfigIndex() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            currentStreamConfigIndex = 0;
        } else {
            currentStreamConfigIndex = -1;
        }
    }

    void cancelRetryTimer() {
        if (retryTimer != null) {
            retryTimer.cancel();
            retryTimer = null;
        }
        if (retryTimerTask != null) {
            retryTimerTask.cancel();
            retryTimerTask = null;
        }
    }

    void cancelIsLiveTimer() {
        if (isLiveTimer != null) {
            isLiveTimer.cancel();
            isLiveTimer = null;
        }
        if (isLiveTimerTask != null) {
            isLiveTimerTask.cancel();
            isLiveTimerTask = null;
        }
    }

    public String getHlsUrl(String entryId) {
        //Application.bambooCdnUrl
        String baseUrl = pvpHelper.getConfig().application.get("bambooCdnUrl").toString();
        return (baseUrl != null && baseUrl != "false" ? baseUrl : sharedPreferences.getString("pvpServerBaseUrl", PvpHelper.PVP_SERVER_BASE_URL)) + ("/api/entry/" + entryId + "/flavors/playlist.m3u8?iid=" + pvpHelper.getConfig().currentInstanceId);
    }

    public String getMpegDashUrl(String entryId) {
        String dashUrl = pvpHelper.getConfig().kaltura.get("serviceUrl") + "p/" + pvpHelper.getPartnerId() + "/sp/" + pvpHelper.getPartnerId() + "00/playManifest/entryId/" + entryId + "/format/mpegdash/protocol/https/a.mpd?iid=" + pvpHelper.getConfig().currentInstanceId;

        return dashUrl;
    }

    public String getDrmLicenseUrl(String entryId) {
        String drmServiceUrl = "";
        try {
            if (pvpHelper.isDrmEnabled()) {
                drmServiceUrl = (String) pvpHelper.getConfig().drm.get("drmServiceUrl");
                drmServiceUrl = drmServiceUrl.replace("{token}", sharedPreferences.getString("token", ""));
                drmServiceUrl = drmServiceUrl.replace("{instanceId}", pvpHelper.getConfig().currentInstanceId);
                drmServiceUrl = drmServiceUrl.replace("{entryId}", entryId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drmServiceUrl;
    }

    public void setPlayerInterface(BambooPlayerInterface playerInterface) {
        this.playerInterface = playerInterface;
    }

    @UiThread
    public void showProgress() {
        progress_bar.setVisibility(View.VISIBLE);
    }
    @UiThread
    public void hideProgress() {
        progress_bar.setVisibility(View.GONE);
    }

    boolean isProgressShowing() {
        return progress_bar.getVisibility() == View.VISIBLE;
    }

    public void showImage(@NonNull final String imageUrl) {
        new Handler(Looper.getMainLooper()).post(() -> {
            try {
                Glide.with(context).load(imageUrl).into(player_image);
                player_image.setVisibility(VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void hideImage() {
        if (entry == null || entry.mediaType != KalturaMediaType.AUDIO.hashCode) {
            new Handler(Looper.getMainLooper()).post(() -> player_image.setVisibility(GONE));
        }
    }

    @UiThread
    private void kalturaPlayerUI() {
        kaltura_player_root.setVisibility(VISIBLE);
        if (pvpHelper.isTvScreen()) {
            kaltura_player_controls.setVisibility(GONE);
        } else {
            kaltura_player_controls.setVisibility(VISIBLE);
        }
        videoView.setVisibility(GONE);
    }

    @Background
    private void startKalturaPlayer() {
        kalturaPlayerUI();

        stop();
        registerPlugins();

        if (entry != null) {
            String entryId = entry.id;
            String entrySrcUrl = video.toString();

            PKMediaEntry pkMediaEntry = new PKMediaEntry();
            pkMediaEntry.setId(entryId);
            pkMediaEntry.setMediaType(PKMediaEntry.MediaEntryType.Unknown);

            List<PKMediaSource> mediaSources = new ArrayList<>();

            PKMediaSource mediaSource = new PKMediaSource()
                    .setId(entryId)
                    .setUrl(entrySrcUrl)
                    .setMediaFormat(PKMediaFormat.valueOfUrl(entrySrcUrl));
            if (pvpHelper.isDrmEnabled()) {
                mediaSource.setDrmData(Collections.singletonList(new PKDrmParams(getDrmLicenseUrl(entryId), PKDrmParams.Scheme.WidevineCENC)));
            }

            mediaSources.add(mediaSource);

            pkMediaEntry.setSources(mediaSources);
            long startPosition = 0;
            if (entry.info != null) {
                startPosition = entry.info.watchTime;
            }
            onMediaLoaded(pkMediaEntry, startPosition);
        }
    }

    private void registerPlugins() {
        PlayKitManager.registerPlugins(context, IMAPlugin.factory, KalturaStatsPlugin.factory);
    }

    private PKPluginConfigs configurePlugins(PKPluginConfigs config) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("delay", 1200);
        config = addIMAPluginConfig(config);
        config = addKalturaStatsPlugin(config);

        return config;
    }

    private PKPluginConfigs addIMAPluginConfig(PKPluginConfigs config) {
        try {
            String adTagUrl = formattedAdTagUrl();
            if (adTagUrl != null && adTagUrl.length() > 0) {
                IMAConfig adsConfig = new IMAConfig().setAdTagURL(adTagUrl);
                config.setPluginConfig(IMAPlugin.factory.getName(), adsConfig);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return config;
    }

    private PKPluginConfigs addKalturaStatsPlugin(PKPluginConfigs config) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;

            JsonObject kalturaStatsConfig = new JsonObject();
            kalturaStatsConfig.addProperty("sessionId", pvpHelper.getConfig().userKs);
            kalturaStatsConfig.addProperty("uiconfId", pvpHelper.getPlayerUiConfId());
            kalturaStatsConfig.addProperty("baseUrl", pvpHelper.getStatsUrl());
            kalturaStatsConfig.addProperty("partnerId", pvpHelper.getPartnerId());
            String entryId = "";
            if (entry != null) {
                entryId = entry.id;
            } else if (liveEntry != null) {
                entryId = liveEntry.id;
            } else if (channel != null) {
                entryId = channel.entry.id;
            }
            kalturaStatsConfig.addProperty("entryId", entryId);
            kalturaStatsConfig.addProperty("timerInterval", 10000); //Timer interval to check progress of the media in milliseconds- recommended value - short media - 10000, long media - 30000
            config.setPluginConfig(KalturaStatsPlugin.factory.getName(), kalturaStatsConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return config;
    }

    protected String formattedAdTagUrl() {

        /**
         * Example add tag url for testing:
         * String adTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator="; //demo tag
         */

        boolean isGoogleIMAEnabled = pvpHelper.isGoogleIMAEnabled();
        String adTagUrl = null;
        if(isGoogleIMAEnabled) {
            adTagUrl = pvpHelper.getAdTagUrl();
        }

        String uniqueId = UUID.randomUUID().toString();

        if (isGoogleIMAEnabled && adTagUrl != null && adTagUrl.length() > 0) {

            String entryId = "";
            String entryName = "";
            String entryDesc = "";

            if (entry != null) {
                entryId = entry.id;
                entryName = entry.name;
                if (entry.description != null) {
                    entryDesc = entry.description;
                }
            } else if (liveEntry != null) {
                entryId = liveEntry.id;
                entryName = liveEntry.name;
                if (liveEntry.description != null) {
                    entryDesc = liveEntry.description;
                }
            } else if (channel != null) {
                entryId = channel.getId();
                entryName = channel.name;
                entryDesc = "";
            }

            HashMap<String, String> adTagPropertiesMap = new HashMap<String, String>();
            adTagPropertiesMap.put("[PLAYER_WIDTH]", "1920");
            adTagPropertiesMap.put("[PLAYER_HEIGHT]", "1080");
            adTagPropertiesMap.put("[MEDIA_ID]", entryId);
            adTagPropertiesMap.put("[MEDIA_TITLE]", entryName);
            adTagPropertiesMap.put("[MEDIA_DESCRIPTION]", entryDesc);
            adTagPropertiesMap.put("[MEDIA_URL]", video.toString());
            adTagPropertiesMap.put("[DEVICE_IDENTIFIER]", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
            adTagPropertiesMap.put("[UUID]", uniqueId);
            adTagPropertiesMap.put("[DEVICE_IP_ADDRESS]", PvpNetworkStatusHelper.getIpAddress());
            adTagPropertiesMap.put("[DEVICE_USER_AGENT]", System.getProperty("http.agent"));
//            adTagPropertiesMap.put("[PAGE_SRC_URL]", "antv://android");
            adTagPropertiesMap.put("[DEVICE_PLATFORM]", "Android");
            adTagPropertiesMap.put("[APPLICATION_BUNDLE]", context.getPackageName());
            adTagPropertiesMap.put("[APPLICATION_NAME]", pvpHelper.getApplicationName());
            adTagPropertiesMap.put("[APPLICATION_VERSION]", pvpHelper.getApplicationVersion());
            adTagPropertiesMap.put("[DEVICE_MOBILE_COUNTRY_CODE]", pvpHelper.getMobileCountryCode());
            adTagPropertiesMap.put("[DEVICE_MOBILE_NETWORK_CODE]", pvpHelper.getMobileNetworkCode());
            adTagPropertiesMap.put("[DEVICE_TIME_ZONE]", TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT));
            adTagPropertiesMap.put("[DEVICE_LOCALE]", Locale.getDefault().getLanguage());
            adTagPropertiesMap.put("[DEVICE_LANGUAGE]", Locale.getDefault().getDisplayLanguage());
            adTagPropertiesMap.put("[DEVICE_DO_NOT_TRACK_FLAG]", "false");

            Set set = adTagPropertiesMap.entrySet();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Map.Entry mentry = (Map.Entry)iterator.next();
                adTagUrl = adTagUrl.replace(mentry.getKey().toString(), mentry.getValue().toString());
            }
        }
        return adTagUrl;
    }

    private void onMediaLoaded(PKMediaEntry mediaEntry, long startPosition) {
        PKMediaConfig mediaConfig = new PKMediaConfig()
                .setMediaEntry(mediaEntry)
                .setStartPosition(startPosition);
        PKPluginConfigs pluginConfig = new PKPluginConfigs();
        if (kalturaPlayer == null) {
            pluginConfig = configurePlugins(pluginConfig);
            kalturaPlayer = PlayKitManager.loadPlayer(context, pluginConfig);
            addPlayerListeners();
            kaltura_player_root.addView(kalturaPlayer.getView());
            kaltura_player_controls.setPlayer(kalturaPlayer);
        }

        kalturaPlayer.prepare(mediaConfig);
        try {
            kalturaPlayer.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        showProgress();
    }

    private void addPlayerListeners() {
        kalturaPlayer.addListener(this, AdEvent.Type.ERROR, event -> {
            Log.d(TAG, "AdEvent.Type.ERROR");
            showProgress();
        });

        kalturaPlayer.addListener(this, AdEvent.Type.CONTENT_PAUSE_REQUESTED, event -> {
            Log.d(TAG, "AdEvent.Type.AD_CONTENT_PAUSE_REQUESTED");
            showProgress();
        });

        kalturaPlayer.addListener(this, AdEvent.Type.CUEPOINTS_CHANGED, event -> {
            Log.d(TAG, "AdEvent.Type.CUEPOINTS_CHANGED");
            AdEvent.AdCuePointsUpdateEvent cuePointsList = (AdEvent.AdCuePointsUpdateEvent) event;
            AdCuePoints adCuePoints = cuePointsList.cuePoints;
            if (adCuePoints != null) {
//                    log.d("Has Postroll = " + adCuePoints.hasPostRoll());
            }
        });

        kalturaPlayer.addListener(this, AdEvent.Type.STARTED, event -> {
            Log.d(TAG, "AdEvent.Type.STARTED");
            hideProgress();
            hideImage();
        });

        kalturaPlayer.addListener(this, AdEvent.Type.RESUMED, event -> {
            Log.d(TAG, "AdEvent.Type.RESUMED");
            hideImage();
            hideProgress();
        });

        kalturaPlayer.addListener(this, AdEvent.Type.ALL_ADS_COMPLETED, event -> {
            Log.d(TAG, "AdEvent.Type.ALL_ADS_COMPLETED");
//                hideProgress();
            try {
                kalturaPlayer.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        kalturaPlayer.addListener(this, AdEvent.Type.SKIPPED, event -> {
            Log.d(TAG, "AdEvent.Type.SKIPPED");
            try {
                kalturaPlayer.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
            showProgress();
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.PLAY, event -> {
            Log.d(TAG, "PlayerEvent.Type.PLAY");
            hideImage();
            hideProgress();
            if (playerInterface != null) {
                if (!playerStarted) {
                    playerStarted = true;
                    playerInterface.onPlayerStart();
                    sendGoogleAnalyticsEvent("Play");
                    restartWatchDurationAnalytics();
                } else {
                    resumeWatchDurationAnalytics();
                }

                playerInterface.onPlayerPlayPause(true);
            }
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.SEEKED, event -> {
            Log.d(TAG, "PlayerEvent.Type.SEEKED");
            if (playerInterface  != null) {
                playerInterface.onPlayerSeeked();
            }
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.PAUSE, event -> {
            Log.d(TAG, "PlayerEvent.Type.PAUSE");
            if (playerInterface  != null) {
                sendGoogleAnalyticsEvent("Pause");
                pauseWatchDurationAnalytics();
                playerInterface.onPlayerPlayPause(false);
            }
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.ENDED, event -> {
            Log.d(TAG, "PlayerEvent.Type.ENDED");
            if (playerInterface != null) {
                playerInterface.onPlayerComplete();
            }
            stopWatchDurationAnalytics();
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.ERROR, event -> {
            Log.d(TAG, "PlayerEvent.Type.ERROR");
            if (playerInterface != null) {
                playerInterface.onPlayerError();
            }
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.STATE_CHANGED, event -> {
            if (event instanceof PlayerEvent.StateChanged) {
                PlayerEvent.StateChanged stateChanged = (PlayerEvent.StateChanged) event;
                Log.d(TAG, "PlayerEvent.Type.STATE_CHANGED : State changed from " + stateChanged.oldState + " to " + stateChanged.newState);

                if(kaltura_player_controls != null){
                    kaltura_player_controls.setPlayerState(stateChanged.newState);
                }
            }
        });

        kalturaPlayer.addListener(this, PlayerEvent.Type.TRACKS_AVAILABLE, event -> {
            Log.d(TAG, "PlayerEvent.Type.TRACKS_AVAILABLE");
            //When the track data available, this event occurs. It brings the info object with it.
            PlayerEvent.TracksAvailable tracksAvailable = (PlayerEvent.TracksAvailable) event;
            //populateSpinnersWithTrackInfo(tracksAvailable.getPKTracks());
        });
    }

    private void updateResumePosition() {
        if (player != null) {
            resumeWindow = player.getCurrentWindowIndex();
            resumePosition = player.isCurrentWindowSeekable() ? Math.max(0, player.getCurrentPosition())
                    : C.TIME_UNSET;
        }
    }

    /**
     * Getting current position from the entry
     * @return - current position in sec
     */
    public int getEntryCurrentPosition() {
        if (kalturaPlayer != null) {
            long position = kalturaPlayer.getCurrentPosition();
            if (position == Consts.POSITION_UNSET) {
                return POSITION_UNSET;
            } else {
                return (int)(position / 1000L);
            }
        }
        return POSITION_UNSET;
    }

    /**
     * Getting duration from the entry
     * @return - current position in sec
     */
    public int getEntryDuration() {
        if (kalturaPlayer != null) {
            long duration = kalturaPlayer.getDuration();
            if (duration == Consts.TIME_UNSET) {
                return TIME_UNSET;
            } else {
                return (int)(duration / 1000L);
            }
        }
        return TIME_UNSET;
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #BANDWIDTH_METER} as a listener to the new
     *     DataSource factory.
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        if (useBandwidthMeter) {
            return new DefaultDataSourceFactory(context, BANDWIDTH_METER,
                    new DefaultHttpDataSourceFactory(Util.getUserAgent(context, pvpHelper.getApplicationName()), BANDWIDTH_METER));
        } else {
            return new DefaultDataSourceFactory(context, null,
                    new DefaultHttpDataSourceFactory(Util.getUserAgent(context, pvpHelper.getApplicationName()), null));
        }
    }

    /**
     * Returns a new HttpDataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #BANDWIDTH_METER} as a listener to the new
     *     DataSource factory.
     * @return A new HttpDataSource factory.
     */
    private HttpDataSource.Factory buildHttpDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultHttpDataSourceFactory(Util.getUserAgent(context, pvpHelper.getApplicationName()), useBandwidthMeter ? BANDWIDTH_METER : null);
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    public void checkIfLive() {
        String id = null;
        if (this.liveEntry != null) {
            id = this.liveEntry.id;
        } else if (this.channel != null && this.channel.entry != null) {
            id = this.channel.entry.id;
        } else if (this.entry != null && this.entry.mediaType == KalturaMediaType.LIVE_STREAM_FLASH.hashCode) {
            id = this.entry.id;
        }
        if (id != null) {
            liveEntryModel.checkIfLive(id, this);
        }
    }

    public MediaInfo getOVPCastMediaInfo() {
        OVPCastBuilder ovpCastBuilder = new OVPCastBuilder()
                .setKs(pvpHelper.getConfig().userKs)
                .setMwEmbedUrl(getHlsUrl(this.entry.id))
                .setPartnerId(pvpHelper.getPartnerId())
                .setUiConfId(pvpHelper.getPlayerUiConfId())
                .setMediaEntryId(this.entry.id)
                .setStreamType(BasicCastBuilder.StreamType.VOD);

        return ovpCastBuilder.build();
    }

    @Nullable
    public String getHlsUrl() {
        if (this.entry != null) {
            if (entry.status == 7 && entry.info != null && entry.info.hlsUrl != null && !entry.info.hlsUrl.isEmpty()) {
                return entry.info.hlsUrl;
            } else {
                return getHlsUrl(this.entry.id);
            }
        } else if (liveEntry != null) {
            return liveEntry.hlsStreamUrl;
        } else if (channel != null) {
            return channel.hlsUrl;
        } else {
            return null;
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE) {
            //view is visible
        } else {
            //onPause() has been called
            if (!pvpHelper.isAllowBackgroundPlayback()) {
                try {
                    pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus) {
            //view is visible
        } else {
            //onPause() has been called
        }
    }

    @Override
    public void configRequestSuccess() {
        startVideo();
    }

    @Override
    public void configRequestFail() {
        if (playerInterface != null) {
            playerInterface.onPlayerError();
        }
    }

    @Override
    public void entryRequestSuccess(PvpEntry entry) {
        playEntry(entry);
    }

    @Override
    public void liveEntryRequestSuccess(PvpLiveEntry entry) {
        playLiveEntry(entry);
    }

    @Override
    public void entryRequestFail() {
        if (playerInterface != null) {
            playerInterface.onPlayerError();
        }
    }

    @Override
    public void liveEntryRequestFail() {
        if (playerInterface != null) {
            playerInterface.onPlayerError();
        }
    }

    @Override
    public void liveEntryIsLiveRequestSuccess(List<PvpResult> results) {
        String id = null;
        int endDate = 0;
        if (this.liveEntry != null) {
            id = this.liveEntry.id;
            endDate = this.liveEntry.endDate;
        } else if (this.channel != null && this.channel.entry != null) {
            id = this.channel.entry.id;
            endDate = this.channel.entry.endDate;
        } else if (this.entry != null && this.entry.mediaType == KalturaMediaType.LIVE_STREAM_FLASH.hashCode) {
            id = this.entry.id;
            endDate = this.entry.endDate;
        }
        if (id != null) {
            for (PvpResult result : results) {
                if (result.id.equals(id)) {
                    boolean isLive = Boolean.parseBoolean(result.result);
                    setIsLive(isLive);
                    if (this.liveEntry != null && !isLive) {
                        showDialogInMainLoop();
                    } else {
                        long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
                        if (endDate > 0 && endDate < currentTime) {
                            showDialogInMainLoop();
                        }
                    }
                    break;
                }
            }
        }
    }

   private void setIsLive(boolean isLive) {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (kaltura_player_controls != null) {
                kaltura_player_controls.setIsLive(isLive);
            }
        });
    }

    private void showDialogInMainLoop() {
        new Handler(Looper.getMainLooper()).post(this::streamEnded);
    }

    private void streamEnded() {
        try {
            if (!streamEndDialog.isShowing()) {
                streamEndDialog.show();
            }
        } catch (Exception e) {}
    }

    @Override
    public void liveEntryCreateRequestSuccess(PvpLiveEntry entry) {}

    @Override
    public void entryRequestSuccess(List<PvpEntry> entries) {}

    @Override
    public void entryRequestSuccessWithMeta(List<PvpEntry> entries, PvpMeta meta) {}

    @Override
    public void liveEntryRequestSuccess(List<PvpLiveEntry> entries) {

    }

    @Override
    public void channelRequestSuccess(PvpChannel channel) {
        playChannel(channel);
    }

    @Override
    public void channelRequestFail() {
        if (playerInterface != null) {
            playerInterface.onPlayerError();
        }
    }

    @Override
    public void channelRequestSuccess(ArrayList<PvpChannel> channels) {}
    @Override
    public void channelEpgRequestSuccess(ArrayList<PvpChannel> channels, int total) {}
    @Override
    public void channelScheduleRequestSuccess(PvpChannel channel) {}
}
