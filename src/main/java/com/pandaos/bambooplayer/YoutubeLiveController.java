package com.pandaos.bambooplayer;

import android.view.View;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;

public class YoutubeLiveController extends AbstractYouTubePlayerListener {

    public YoutubeLiveController(View controlsView, YouTubePlayer player) {
        YouTubePlayerTracker playerTracker = new YouTubePlayerTracker();
        player.addListener(playerTracker);

        controlsView.findViewById(R.id.root).setOnClickListener((view) -> {
            if (playerTracker.getState() == PlayerConstants.PlayerState.PLAYING) {
                player.pause();
            } else {
                player.play();
            }
        });
    }

}
