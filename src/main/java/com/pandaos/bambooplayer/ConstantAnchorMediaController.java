package com.pandaos.bambooplayer;

import android.content.Context;
import android.view.View;
import android.widget.MediaController;

/**
 * Created by elad on 10/11/2016.
 */

public class ConstantAnchorMediaController extends MediaController {

    public ConstantAnchorMediaController(Context context, View anchor) {
        super(context);
        super.setAnchorView(anchor);
    }

    @Override
    public void setAnchorView(View view) {
    }
}
