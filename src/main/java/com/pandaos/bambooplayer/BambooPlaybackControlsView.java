package com.pandaos.bambooplayer;

import android.content.Context;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.core.graphics.ColorUtils;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kaltura.playkit.PKLog;
import com.kaltura.playkit.Player;
import com.kaltura.playkit.PlayerState;
import com.pandaos.pvpclient.models.PvpColors;
import com.pandaos.pvpclient.models.PvpColors_;
import com.pandaos.pvpclient.models.PvpFontWeight;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpHelper_;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.UiThread;

import java.util.Formatter;
import java.util.Locale;

/**
 * Created by orenkosto on 2/26/17.
 */

public class BambooPlaybackControlsView extends LinearLayout implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    public interface BambooPlaybackControlsViewInterface {
        void setDvrForward();
        void setDvrRewind();
        void playOrPauseVideoDVR();
        void toggleFullScreenMode(boolean isFullscreen);
    }

    public BambooPlaybackControlsViewInterface playbackControlsViewInterface;

    public enum ControlsType {
        DEFAULT,
        LIVE,
        PLAY,
        PAUSE,
        FORWARD,
        REWIND
    }

    private ControlsType controlsType = ControlsType.DEFAULT;

    private static final PKLog log = PKLog.get("PlaybackControlsView");
    private static final int PROGRESS_BAR_MAX = 100;
    private static final long PROGRESS_UPDATE_DELAY = 1000;
    private static final int CONTROLS_REVEAL_TIME = 5;

    PvpHelper pvpHelper;
    PvpColors pvpColors;
    SharedPreferences_ sharedPreferences;

    private Player player;
    private PlayerState playerState;

    private SimpleExoPlayerView nativePlayer;
    public MediaPlayer nativeMediaPlayer;

    private Formatter formatter;
    private StringBuilder formatBuilder;

    private SeekBar seekBar;
    private TextView tvCurTime, tvTime, live_indicator_text;
    private ImageButton btnPlay, btnPause, btnFastForward, btnRewind, btnNext, btnPrevious, btnFullScreen;
    private ImageView live_indicator_img;

    private static GoogleAnalytics googleAnalytics;
    private static Tracker googleAnalyticsTracker;
    private FirebaseAnalytics mFirebaseAnalytics;
    boolean sent25Percent = false;
    boolean sent50Percent = false;
    boolean sent75Percent = false;
    boolean sent100Percent = false;

    private boolean dragging = false;
    boolean catchUpMobile = false;

    private int hideCountdown = CONTROLS_REVEAL_TIME;
    private boolean isFullScreen = false;

    private Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    public BambooPlaybackControlsView(Context context) {
        this(context, null);
    }

    public BambooPlaybackControlsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BambooPlaybackControlsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        pvpHelper = PvpHelper_.getInstance_(context);
        pvpColors = PvpColors_.getInstance_(context);
        sharedPreferences = new SharedPreferences_(context);
        LayoutInflater.from(context).inflate(R.layout.bamboo_playback_controls, this);
        formatBuilder = new StringBuilder();
        formatter = new Formatter(formatBuilder, Locale.getDefault());
        initPlaybackControls();
        setupGoogleAnalytics(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void setCatchUpMobile(boolean catchUpMobile) {
        this.catchUpMobile = catchUpMobile;
        if(catchUpMobile) {
            btnFastForward.setVisibility(VISIBLE);
            btnRewind.setVisibility(VISIBLE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT,
                    0.5f
            );
            LinearLayout indicator_linear_layout = (LinearLayout) this.findViewById(R.id.indicator_linear_layout);
            indicator_linear_layout.setLayoutParams(param);
        }
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        isFullScreen = fullScreen;
        btnFullScreen.setImageDrawable(getContext().getDrawable(isFullScreen ? R.drawable.ic_fullscreen_exit : R.drawable.ic_fullscreen));
    }

    private void initPlaybackControls() {

        btnPlay = (ImageButton) this.findViewById(R.id.play);
        btnPause = (ImageButton) this.findViewById(R.id.pause);
        btnFastForward = (ImageButton) this.findViewById(R.id.ffwd);
        btnRewind = (ImageButton) this.findViewById(R.id.rew);
        btnNext = (ImageButton) this.findViewById(R.id.next);
        btnPrevious = (ImageButton) this.findViewById(R.id.prev);
        live_indicator_img = (ImageView) this.findViewById(R.id.live_indicator_img);
        btnFullScreen = this.findViewById(R.id.btnFullScreen);

        btnPlay.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnFastForward.setOnClickListener(this);
        btnRewind.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        btnFullScreen.setOnClickListener(this);

        seekBar = (SeekBar) this.findViewById(R.id.mediacontroller_progress);
        seekBar.setOnSeekBarChangeListener(this);

        tvCurTime = (TextView) this.findViewById(R.id.time_current);
        tvTime = (TextView) this.findViewById(R.id.time);
        live_indicator_text = (TextView) this.findViewById(R.id.live_indicator_text);

        this.getRootView().setBackgroundColor(ColorUtils.setAlphaComponent(pvpColors.getParsingColorFromConfig(PvpColors.BACKGROUND_COLOR), 50));
        btnPlay.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnPause.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnFastForward.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnRewind.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnNext.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnPrevious.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        btnFullScreen.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        setFullScreen(isFullScreen);

        tvCurTime.setTextColor(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_HIGHLIGHT_COLOR));
        tvTime.setTextColor(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_HIGHLIGHT_COLOR));

        if (pvpHelper.isCustomFontsEnabled()) {
            tvCurTime.setTypeface(pvpHelper.getCustomFont(PvpFontWeight.REGULAR));
            tvTime.setTypeface(pvpHelper.getCustomFont(PvpFontWeight.REGULAR));
            live_indicator_text.setTypeface(pvpHelper.getCustomFont(PvpFontWeight.REGULAR));
        }

        seekBar.getProgressDrawable().setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR), PorterDuff.Mode.SRC_IN);
        seekBar.getThumb().setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR), PorterDuff.Mode.SRC_IN);

        if (BambooPlaybackControlsViewInterface.class.isAssignableFrom(this.getClass())) {
            setBambooPlaybackControlsViewInterface((BambooPlaybackControlsViewInterface) this);
        }
    }

    public void setBambooPlaybackControlsViewInterface(BambooPlaybackControlsViewInterface playerInterface) {
        this.playbackControlsViewInterface = playerInterface;
    }

    private void setupGoogleAnalytics(Context context) {
        String trackingId = pvpHelper.googleAnalyticsTrackingId();
        if (trackingId != null) {
            googleAnalytics = GoogleAnalytics.getInstance(context);
            googleAnalyticsTracker = googleAnalytics.newTracker(trackingId);
        }
        sent25Percent = false;
        sent50Percent = false;
        sent75Percent = false;
        sent100Percent = false;
    }

    private void sendGoogleAnalyticsEvent(String action) {
        if (googleAnalyticsTracker != null) {
            googleAnalyticsTracker.send(new HitBuilders
                    .EventBuilder()
                    .setCategory("BambooPlayer")
                    .setAction(action)
                    .setValue(1)
                    .build());
        }

        if (mFirebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            bundle.putString("action", action);
            bundle.putString("user_udid", sharedPreferences.userDeviceId().get());
            if(pvpHelper.getCurrentUser()!=null) {
                bundle.putString("userId", pvpHelper.getCurrentUser() != null ? pvpHelper.getCurrentUser().id : null);
            }
            mFirebaseAnalytics.logEvent("bamboo_player_events", bundle);
        }
    }

    private void updateProgress() {
        long duration = C.TIME_UNSET;
        long position = C.POSITION_UNSET;
        long bufferedPosition = 0;

        if(player != null){
            duration = player.getDuration();
            position = player.getCurrentPosition();
            bufferedPosition = player.getBufferedPosition();
        } else if (nativePlayer != null) {
            duration = nativePlayer.getPlayer().getDuration();
            position = nativePlayer.getPlayer().getCurrentPosition();
            bufferedPosition = (nativePlayer.getPlayer().getBufferedPercentage() / 100) * duration;
        }

        if(duration != C.TIME_UNSET){
            //log.d("updateProgress Set Duration:" + duration);
            tvTime.setText(stringForTime(duration));
            if (!dragging && position != C.POSITION_UNSET) {
                //log.d("updateProgress Set Position:" + position);
                int progressBarValue = progressBarValue(position);
                tvCurTime.setText(stringForTime(position));
                seekBar.setProgress(progressBarValue);
                if (progressBarValue >= ((float) PROGRESS_BAR_MAX * 0.25) && !sent25Percent) {
                    sent25Percent = true;
                    sendGoogleAnalyticsEvent("PlayReached25");
                } else if (progressBarValue >= ((float) PROGRESS_BAR_MAX * 0.5) && !sent50Percent) {
                    sent50Percent = true;
                    sendGoogleAnalyticsEvent("PlayReached50");
                } else if (progressBarValue >= ((float) PROGRESS_BAR_MAX * 0.75) && !sent75Percent) {
                    sent75Percent = true;
                    sendGoogleAnalyticsEvent("PlayReached75");
                } else if (progressBarValue >= ((float) PROGRESS_BAR_MAX * 0.9) && !sent100Percent) {
                    sent100Percent = true;
                    sendGoogleAnalyticsEvent("PlayReached100");
                }
            }
        } else if (!dragging) {
            tvTime.setText("");
            tvCurTime.setText("");
            seekBar.setProgress(0);
        }
        if (!dragging) {
            if (hideCountdown > 0) {
                hideCountdown--;
            } else {
                showControls(false);
            }
        }

        seekBar.setSecondaryProgress(progressBarValue(bufferedPosition));
        // Remove scheduled updates.
        removeCallbacks(updateProgressAction);
        // Schedule an update if necessary.
        if (playerState != PlayerState.IDLE) {
            postDelayed(updateProgressAction, PROGRESS_UPDATE_DELAY);
        }

        if (player != null) {
            if (player.isPlaying()) {
                btnPlay.setVisibility(GONE);
                btnPause.setVisibility(VISIBLE);
            } else {
                btnPlay.setVisibility(VISIBLE);
                btnPause.setVisibility(GONE);
            }
        } else if (nativePlayer != null) {
            if (nativePlayer.getPlayer().getPlayWhenReady()) {
                btnPlay.setVisibility(GONE);
                btnPause.setVisibility(VISIBLE);
            } else {
                btnPlay.setVisibility(VISIBLE);
                btnPause.setVisibility(GONE);
            }
        }
    }

    private int progressBarValue(long position) {
        int progressValue = 0;
        if(player != null){
            long duration = player.getDuration();
            if (duration > 0) {
                progressValue = (int) ((position * PROGRESS_BAR_MAX) / duration);
            }
        } else if (nativePlayer != null) {
            long duration = nativePlayer.getPlayer().getDuration();
            if (duration > 0) {
                progressValue = (int) ((position * PROGRESS_BAR_MAX) / duration);
            }
        }

        return progressValue;
    }

    public void seekBy(int offsetSeconds) {
        if (this.controlsType == ControlsType.DEFAULT) {
            long duration;
            long offsetMs = offsetSeconds * 1000L;
            if(player != null) {
                duration = player.getDuration();
                player.seekTo(Math.max(0, Math.min(player.getCurrentPosition() + offsetMs, duration)));
            } else if (nativePlayer != null) {
                duration = nativePlayer.getPlayer().getDuration();
                nativePlayer.getPlayer().seekTo(Math.max(0, Math.min(nativePlayer.getPlayer().getCurrentPosition() + offsetMs, duration)));
            }
            updateProgress();
        }
    }

    private long positionValue(int progress) {
        long positionValue = 0;
        if(player != null){
            long duration = player.getDuration();
            positionValue = (duration * progress) / PROGRESS_BAR_MAX;
        } else if (nativePlayer != null) {
            long duration = nativePlayer.getPlayer().getDuration();
            positionValue = (duration * progress) / PROGRESS_BAR_MAX;
        }

        return positionValue;
    }

    private String stringForTime(long timeMs) {

        long totalSeconds = (timeMs + 500) / 1000;
        long seconds = totalSeconds % 60;
        long minutes = (totalSeconds / 60) % 60;
        long hours = totalSeconds / 3600;
        formatBuilder.setLength(0);
        return hours > 0 ? formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString()
                : formatter.format("%02d:%02d", minutes, seconds).toString();
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.player.getView().setOnTouchListener((v, event) -> {
            showControls(true);
            return false;
        });
        this.nativePlayer = null;
        this.nativeMediaPlayer = null;
    }

    public void setPlayer(SimpleExoPlayerView player) {
        this.nativePlayer = player;
        this.nativePlayer.setOnTouchListener((v, event) -> {
            showControls(true);
            return false;
        });
        this.player = null;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
        updateProgress();
    }

    public void showControls(final boolean show) {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (show) {
                setVisibility(VISIBLE);
                if (getAlpha() != 1) {
                    animate().alpha(1).setDuration(500).start();
                }
                hideCountdown = CONTROLS_REVEAL_TIME;
            } else {
//            this.setVisibility(GONE);
                if (getAlpha() != 0) {
                    animate().alpha(0).setDuration(500).start();
                }
                hideCountdown = 0;
            }
        });
    }

    public void setControlsType(ControlsType type) {
        this.controlsType = type;

        seekBar.setVisibility(VISIBLE);
        tvTime.setVisibility(GONE);
        live_indicator_text.setVisibility(VISIBLE);
        live_indicator_img.setVisibility(VISIBLE);
        btnPlay.setVisibility(GONE);
        btnPause.setVisibility(GONE);

        if(!catchUpMobile) {
            btnFastForward.setVisibility(GONE);
            btnRewind.setVisibility(GONE);
        }

        btnFullScreen.setVisibility(pvpHelper.isTvScreen() ? View.GONE : View.VISIBLE);

        if (type == ControlsType.DEFAULT) {
            tvTime.setVisibility(VISIBLE);
            btnPause.setVisibility(VISIBLE);
            live_indicator_text.setVisibility(GONE);
            live_indicator_img.setVisibility(GONE);
            findViewById(R.id.indicator_linear_layout).setVisibility(VISIBLE);
        } else if (type == ControlsType.LIVE || type == ControlsType.PLAY || type == ControlsType.REWIND || type == ControlsType.FORWARD) {
            btnPause.setVisibility(VISIBLE);
        } else if (type == ControlsType.PAUSE) {
            btnPlay.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (this.getAlpha() > 0) {
            try {
                if (viewId == R.id.btnFullScreen) {
                    playbackControlsViewInterface.toggleFullScreenMode(isFullScreen);
                }
                if(catchUpMobile) {
                    if (viewId == R.id.play) {
                        btnPlay.setVisibility(GONE);
                        btnPause.setVisibility(VISIBLE);
                        playbackControlsViewInterface.playOrPauseVideoDVR();
                    } else if (viewId == R.id.pause) {
                        btnPlay.setVisibility(VISIBLE);
                        btnPause.setVisibility(GONE);
                        playbackControlsViewInterface.playOrPauseVideoDVR();
                    } else if (viewId == R.id.ffwd) {
                        playbackControlsViewInterface.setDvrForward();
                    } else if (viewId == R.id.rew) {
                        playbackControlsViewInterface.setDvrRewind();
                    }
                } else {
                    if (viewId == R.id.play) {
                        if (player != null) {
                            player.play();
                        } else if (nativeMediaPlayer != null) {
                            nativeMediaPlayer.start();
                        } else if (nativePlayer != null) {
                            removeCallbacks(updateProgressAction);
                            nativePlayer.getPlayer().setPlayWhenReady(true);
                        }
                        btnPlay.setVisibility(GONE);
                        btnPause.setVisibility(VISIBLE);
                    } else if (viewId == R.id.pause) {
                        if (player != null) {
                            player.pause();
                        } else if (nativeMediaPlayer != null) {
                            nativeMediaPlayer.pause();
                        } else if (nativePlayer != null) {
                            nativePlayer.getPlayer().setPlayWhenReady(false);
                        }
                        btnPlay.setVisibility(VISIBLE);
                        btnPause.setVisibility(GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        showControls(true);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            tvCurTime.setText(stringForTime(positionValue(progress)));
        }
    }


    public void onStartTrackingTouch(SeekBar seekBar) {
        dragging = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        dragging = false;
        if (this.getAlpha() > 0 && player != null) {
            player.seekTo(positionValue(seekBar.getProgress()));
        } else if(this.getAlpha() > 0 && nativePlayer != null) {
            nativePlayer.getPlayer().seekTo(positionValue(seekBar.getProgress()));
        }
        showControls(true);
    }

    @UiThread
    public void setIsLive(final boolean isLive) {
        live_indicator_text.setText(isLive ? getContext().getString(R.string.live) : getContext().getString(R.string.offline));
        live_indicator_img.setImageResource(isLive ? R.drawable.live_indicator : R.drawable.offline_indicator);
    }

    @UiThread
    public void setIndicatorText(final String indicatorText, final boolean displayLiveIndicator) {
        live_indicator_text.setText(indicatorText);
        live_indicator_img.setImageResource(displayLiveIndicator ? R.drawable.live_indicator : R.drawable.offline_indicator);
    }

    @UiThread
    public void setSeekBar(boolean isVisible) {
        if(isVisible) {
            seekBar.setVisibility(VISIBLE);
        } else {
            seekBar.setVisibility(GONE);
        }
    }

    public void release() {
        removeCallbacks(updateProgressAction);
    }

    public void resume() {
        updateProgress();
    }
}
