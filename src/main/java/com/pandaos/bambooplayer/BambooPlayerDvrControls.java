package com.pandaos.bambooplayer;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pandaos.pvpclient.models.PvpColors;
import com.pandaos.pvpclient.models.PvpColors_;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpHelper_;

/**
 * Created by Aviya on 09/05/18.
 */

public class BambooPlayerDvrControls extends LinearLayout implements View.OnClickListener {

    public enum ControlsType {
        DEFAULT,
        LIVE,
        DVR_PLAY,
        DVR_PAUSE,
        DVR_REWIND,
        DVR_FORWARD,
        DVR_MOBILE_PLAY,
        DVR_MOBILE_PAUSE
    }

    private static final int CONTROLS_REVEAL_TIME = 3;

    PvpHelper pvpHelper;
    PvpColors pvpColors;

    private ImageButton bigBtnPlay, bigBtnPause, bigBtnFastForward, bigBtnRewind;

    private boolean dragging = false;

    protected Handler taskHandler = new Handler();
    protected void setTimer() {
        Runnable t = () -> {
            dragging = false;
            updateProgress();
        };
        taskHandler.postAtTime(t, SystemClock.uptimeMillis() + (CONTROLS_REVEAL_TIME * 1000));
    }

    public BambooPlayerDvrControls(Context context) {
        this(context, null);
    }

    public BambooPlayerDvrControls(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BambooPlayerDvrControls(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        pvpHelper = PvpHelper_.getInstance_(context);
        pvpColors = PvpColors_.getInstance_(context);
        LayoutInflater.from(context).inflate(R.layout.bamboo_player_dvr_controls, this);
        initControls();
    }

    private void initControls() {
        bigBtnPlay = this.findViewById(R.id.big_play);
        bigBtnPause = this.findViewById(R.id.big_pause);
        bigBtnFastForward = this.findViewById(R.id.big_ffwd);
        bigBtnRewind = this.findViewById(R.id.big_rew);

        //this.getRootView().setBackgroundColor(ColorUtils.setAlphaComponent(pvpColors.getParsingColorFromConfig(PvpColors.BACKGROUND_COLOR), 50));

        //Big buttons
        bigBtnPlay.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        bigBtnPause.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        bigBtnFastForward.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
        bigBtnRewind.setColorFilter(pvpColors.getParsingColorFromConfig(PvpColors.PRIMARY_COLOR));
    }

    private void updateProgress() {
        if (!dragging) {
            showControls(false);
        }
    }

    public void showControls(final boolean show) {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (show) {
                dragging = true;
                setVisibility(VISIBLE);
                setTimer();
                if (getAlpha() != 1) {
                    animate().alpha(1).setDuration(500).start();
                }
            } else {
                if (getAlpha() != 0) {
                    animate().alpha(0).setDuration(500).start();
                }
            }
        });
    }

    public void setControlsType(ControlsType type) {
        bigBtnPlay.setVisibility(GONE);
        bigBtnPause.setVisibility(GONE);
        bigBtnRewind.setVisibility(GONE);
        bigBtnFastForward.setVisibility(GONE);
        if(type == ControlsType.DVR_PLAY) {
            bigBtnPlay.setVisibility(VISIBLE);
        } else if(type == ControlsType.DVR_PAUSE) {
            bigBtnPause.setVisibility(VISIBLE);
        } else if(type == ControlsType.DVR_REWIND) {
            bigBtnRewind.setVisibility(VISIBLE);
        } else if(type == ControlsType.DVR_FORWARD) {
            bigBtnFastForward.setVisibility(VISIBLE);
        } else if(type == ControlsType.DVR_MOBILE_PAUSE) {
            bigBtnPlay.setVisibility(VISIBLE);
            bigBtnRewind.setVisibility(VISIBLE);
            bigBtnFastForward.setVisibility(VISIBLE);
        } else if(type == ControlsType.DVR_MOBILE_PLAY) {
            bigBtnPause.setVisibility(VISIBLE);
            bigBtnRewind.setVisibility(VISIBLE);
            bigBtnFastForward.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (this.getAlpha() > 0 && !pvpHelper.isTvScreen()) {
            try {
                if (viewId == R.id.big_play) {
                    //set dvr pause and
                    setControlsType(ControlsType.DVR_MOBILE_PAUSE);
                } else if (viewId == R.id.big_pause) {
                    //set dvr pause and
                    setControlsType(ControlsType.DVR_MOBILE_PLAY);
                } else if (viewId == R.id.big_ffwd) {
                    //set dvr forward
                } else if (viewId == R.id.big_rew) {
                    //set dvr rewind
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        showControls(true);
    }
}
